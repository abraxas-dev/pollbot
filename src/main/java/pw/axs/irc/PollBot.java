package pw.axs.irc;

import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

public class PollBot extends ListenerAdapter<PircBotX> {
    private PircBotX bot;

    private PollBot() {}

    private PollBot(final PircBotX bot) {
        this.bot = bot;
    }

    public static PollBot configure() {
        final PollBot pollBot = new PollBot();
        final Configuration<PircBotX> configuration = new Configuration.Builder<PircBotX>()
                .setServerHostname("irc.abraxas.pw")
                .setServerPassword("abraxas")
                .setServerPort(6667)
                .setAutoNickChange(true)
                .setName("PollBot")
                .addAutoJoinChannel("##")
                .addListener(pollBot)
                .buildConfiguration();
        pollBot.setBot(new PircBotX(configuration));
        return pollBot;
    }

    @Override
    public void onMessage(final MessageEvent event) throws Exception {
        event.respond("DERP");
    }

    public PircBotX getBot() {
        return bot;
    }

    private void setBot(final PircBotX bot) {
        this.bot = bot;
    }
}
