package pw.axs.irc;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class PollBotApplication extends Application<WebConfiguration> {
    public static void main(String[] args) throws Exception {
        new PollBotApplication().run(args);
    }

    @Override
    public void initialize(final Bootstrap<WebConfiguration> webConfigurationBootstrap) {
        //
    }

    @Override
    public void run(final WebConfiguration webConfiguration, final Environment environment) throws Exception {
        PollBot bot = PollBot.configure();
        bot.getBot().startBot();
    }
}
